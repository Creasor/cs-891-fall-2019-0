package assignment1bTests.requiredTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import edu.vanderbilt.imagecrawler.utils.Assignment1bTests;

/**
 * REQUIRED test suite for this assignment.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(Assignment1bTests.class)
public class RequiredTests {
}
