package requiredTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import edu.vanderbilt.imagecrawler.utils.Assignment1aTests;

/**
 * REQUIRED test suite for this assignment.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(Assignment1aTests.class)
public class Assignment1aRequiredTestSuite {
}
